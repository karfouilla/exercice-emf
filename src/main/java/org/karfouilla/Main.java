package org.karfouilla;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.*;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import java.io.IOException;
import java.util.Iterator;
import java.util.Stack;
import java.util.function.BiConsumer;

public class Main {
    static final String METAMODEL_PATH = "https://raw.githubusercontent.com/dartdesigner/Dart-Designer/master/bundles/org.obeonetwork.dsl.dart/model/dart.ecore";
    static final String MODEL_PATH = "https://raw.githubusercontent.com/dartdesigner/Dart-Designer/master/bundles/org.obeonetwork.dsl.dart/model/dartlang.dartspec";
    static final String MODEL_PKG_NSURI = "http://www.obeo.fr/dsl/2014/dart";

    static final URI METAMODEL_URI = URI.createURI(METAMODEL_PATH);
    static final URI MODEL_URI = URI.createURI(MODEL_PATH);

    private static void loadECore(ResourceSet resourceSet, URI uri) throws IOException {
        Resource res = new EcoreResourceFactoryImpl().createResource(uri);
        res.load(null);

        for(EObject content : res.getContents()) {
            if(content instanceof EPackage) {
                EPackage pkg = (EPackage) content;
                resourceSet.getPackageRegistry().put(pkg.getNsURI(), pkg);
            }
        }
    }
    private static EList<EObject> loadXMI(ResourceSet resourceSet, URI uri) throws IOException {
        Resource resData = resourceSet.getResource(uri, true);
        resData.load(null);
        return resData.getContents();
    }

    private static EObject createDartClass(ResourceSet resourceSet, String className) {
        EPackage pkg = resourceSet.getPackageRegistry().getEPackage(MODEL_PKG_NSURI);

        // metadata
        EClass classEClass = (EClass) pkg.getEClassifier("Class");
        EStructuralFeature attrClassName = classEClass.getEStructuralFeature("name");

        // data
        EObject dartClass = pkg.getEFactoryInstance().create(classEClass);
        dartClass.eSet(attrClassName, className);

        return dartClass;
    }
    private static EList<EObject> getProjectPackages(EObject project) {
        EStructuralFeature attrProjectPackages = project.eClass().getEStructuralFeature("packages");
        return (EList<EObject>)project.eGet(attrProjectPackages);
    }
    private static EList<EObject> getPackageAssets(EObject pkg) {
        EStructuralFeature attrPackageAssets = pkg.eClass().getEStructuralFeature("assets");
        return (EList<EObject>)pkg.eGet(attrPackageAssets);
    }

    // Parcours d'un arbre de EObject
    private static void forTree(EList<EObject> elements, BiConsumer<Integer, EObject> action) {
        Stack<Iterator<EObject>> stack = new Stack<>();
        stack.push(elements.iterator());
        while(!stack.isEmpty()) {
            if(stack.peek().hasNext()) {
                EObject element = stack.peek().next();
                action.accept(stack.size()-1, element);
                stack.push(element.eContents().iterator());
            } else {
                stack.pop();
            }
        }
    }

    private static void printElement(int depth, EObject element) {
        EClass eClass = element.eClass();
        EStructuralFeature nameFeature = eClass.getEStructuralFeature("name");

        String className = eClass.getName();
        String objectName;

        if(nameFeature != null) {
            if(nameFeature.isUnique()) {
                objectName = '"' + element.eGet(nameFeature).toString() + '"';
            } else {
                objectName = "<`name` is not unique>";
            }
        } else {
            objectName = "<no `name`>";
        }

        System.out.println("  ".repeat(depth) + className + " " + objectName);
    }

    public static void main(String[] args) {
        // initialisation du ResourceSet
        ResourceSet resourceSet = new ResourceSetImpl();
        resourceSet
                .getResourceFactoryRegistry()
                .getExtensionToFactoryMap()
                .put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
        try {
            // Chargement du meta-modèle
            loadECore(resourceSet, METAMODEL_URI);
            // Chargement du modèle
            EList<EObject> model = loadXMI(resourceSet, MODEL_URI);

            // Nombre d'éléments dans le modèle
            final int[] count = {0};
            forTree(model, (depth, element) -> {
                ++count[0];
            });
            System.out.println("Nombre d'éléments directe : "+model.size());
            System.out.println("Nombre d'éléments récursivement : "+count[0]);
            System.out.println();

            // Affichage des éléments du modèle
            System.out.println("-".repeat(80));
            System.out.println("-".repeat(25)+" Modèle avant ajout d'élément "+"-".repeat(25));
            System.out.println("-".repeat(80));
            forTree(model, Main::printElement);
            System.out.println("-".repeat(80));
            System.out.println();

            // Ajout d'un élément
            EObject dartClass = createDartClass(resourceSet, "Boolean");

            EObject project = model.get(0);
            EList<EObject> packages = getProjectPackages(project);
            EList<EObject> assets = getPackageAssets(packages.get(0));

            assets.add(dartClass);

            // Affichage
            System.out.println("-".repeat(80));
            System.out.println("-".repeat(25)+" Modèle après ajout d'élément "+"-".repeat(25));
            System.out.println("-".repeat(80));
            forTree(model, Main::printElement);
            System.out.println("-".repeat(80));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
